import React from "react";
//Components
import HomeNews from "../components/homeSections/HomeNews";
import Homehelp from "../components/homeSections/HomeHelp";
import HomeHeader from "../components/homeSections/HomeHeader";
export default function HomePage() {
  return (
    <div className="homePage">
      <HomeHeader />
      <HomeNews />
      <Homehelp />
    </div>
  );
}
