import React, { Component } from "react";
import db from "../data/db.json";
import { Container, Row, Col } from "react-bootstrap";
import { Button, Card, Dropdown, Menu, Pagination } from "antd";
import { Link } from "react-router-dom";
import { Form, Select, DatePicker, Input } from "antd";
//Components
import PreLoading from "../containers/PreLoading";
import sort from "../assets/images/sort.png";
const numEachPage = 12;
export default class NewsListing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      newsData: [],
      maxValue: numEachPage,
      minValue: 0,
      currentPage: 1,
      selectedId: null,
      newsCategories: [],
      searchValue: ""
    };
  }
  handleSelectId = (value, e) => {
    if (e.id === "all") {
      this.setState({
        newsData: db.articles
      });
    } else {
      this.setState(
        { selectedId: e.id, loading: true },
        () =>
          this.setState({
            newsData: db.articles.filter(
              c =>
                c.sourceID === e.id &&
                c.description
                  .toLowerCase()
                  .includes(this.state.searchValue.toLowerCase())
            )
          }),
        setTimeout(() => {
          this.setState({ loading: false });
        }, 1000)
      );
    }
  };

  handleSearch = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ [name]: value });
  };
  search = e => {
    this.setState({
      loading: true,
      newsData: this.state.newsData.filter(
        n =>
          n.description.toLowerCase().includes(e.toLowerCase()) &&
          n.sourceID === this.state.selectedId
      )
    });
    setTimeout(() => {
      this.setState({ loading: false });
    }, 1000);
  };
  componentDidMount() {
    this.setState({ newsData: db.articles, newsCategories: db.sourceCategory });
  }
  handleChangePage = page => {
    window.scrollTo(0, 0);
    this.setState({
      currentPage: page,
      minValue: (page - 1) * numEachPage,
      maxValue: page * numEachPage
    });
  };
  render() {
    console.log(this.state.selectedId);
    return (
      <div className="newsListing pt-3">
        <Container className="homeNews">
          <Row>
            <Col xs={8} className="prevLinks">
              <Link to="/" className="homeLink">
                Home
              </Link>{" "}
              <span className="px-3">></span>{" "}
              <Link to="/news" className="newsLink">
                News
              </Link>
              <h1> News</h1>
            </Col>
          </Row>
          <Form layout="vertical">
            <Row>
              <Col md={6} lg={2}>
                <Form.Item name="fromDate" label="From">
                  <DatePicker />
                </Form.Item>
              </Col>{" "}
              <Col md={6} lg={2}>
                <Form.Item name="toDate" label="To">
                  <DatePicker />
                </Form.Item>
              </Col>{" "}
              <Col md={6} lg={2}>
                <Form.Item name="category" label="Category">
                  <Select
                    size="large"
                    virtual={false}
                    showSearch
                    allowClear
                    onChange={this.handleSelectId}
                    value={this.state.selectedId}
                    placeholder="Select"
                    getPopupContainer={trigger => trigger.parentNode}
                  >
                    {" "}
                    <Select.Option
                      className="selectgroup"
                      value={"all"}
                      key={"all"}
                      id={"all"}
                    >
                      All
                    </Select.Option>
                    {this.state.newsCategories &&
                    this.state.newsCategories.length !== 0
                      ? this.state.newsCategories.map((n, index) => (
                          <Select.Option
                            className="selectgroup"
                            value={n.name}
                            key={n.id}
                            id={n.id}
                          >
                            {n.name}
                          </Select.Option>
                        ))
                      : null}
                  </Select>
                </Form.Item>
              </Col>
              <Col md={6} lg={4}>
                <Form.Item name="searchValue" label="Search">
                  <Input.Search
                    placeholder="Search Services"
                    onSearch={this.search}
                    name="searchValue"
                    enterButton
                    value={this.state.searchValue}
                    onChange={this.handleSearch}
                  />
                </Form.Item>
              </Col>
              <Col
                sm={2}
                style={{
                  margin: "auto",
                  textAlign: "right",
                  fontSize: "20px",
                  color: "#8dc043"
                }}
              >
                Sort by
                <img src={sort} className="pl-3" alt="sortIcon" />
              </Col>
            </Row>
          </Form>
          {this.state.loading ? (
            <PreLoading />
          ) : (
            <>
              <Row gutter={16} type="flex">
                {this.state.newsData && this.state.newsData.length !== 0 ? (
                  this.state.newsData.map(
                    (n, index) =>
                      index >= this.state.minValue &&
                      index < this.state.maxValue && (
                        <Col sm={12} md={6} lg={4} key={n.id} className="py-3">
                          <Card
                            style={{ height: "100%" }}
                            hoverable
                            cover={
                              <img
                                alt="example"
                                src={n.urlToImage}
                                style={{ height: "160px" }}
                              />
                            }
                          >
                            <p className="favIcons">
                              <i
                                className="far fa-heart"
                                onClick={() => this.addToFav(n)}
                              ></i>

                              <Dropdown
                                overlay={
                                  <Menu>
                                    <Menu.Item>
                                      <i className="fab fa-facebook-f"></i>
                                      <i className="fab fa-twitter"></i>
                                      <i className="fas fa-envelope"></i>
                                    </Menu.Item>
                                  </Menu>
                                }
                                trigger={["click"]}
                              >
                                <i className="fas fa-share-alt"></i>
                              </Dropdown>
                            </p>
                            <h4 className="cardTitle">{n.title}</h4>
                            <Button className="newsBtn">
                              <Link to={`/news/${n.id}`}>News</Link>
                            </Button>
                            <p className="cardDesc">
                              {n.description.slice(0, 50)}
                            </p>
                            <p className="cardDate">
                              <i className="far fa-calendar-alt pr-3"></i>
                              {new Date(n.publishedAt).toDateString()}
                            </p>
                          </Card>
                        </Col>
                      )
                  )
                ) : (
                  <h2 className="text-center">No results</h2>
                )}
              </Row>
              <Pagination
                className="py-4"
                pageSize={numEachPage}
                current={this.state.currentPage}
                total={this.state.newsData.length}
                onChange={this.handleChangePage}
                style={{ bottom: "0px" }}
              />
            </>
          )}
        </Container>
      </div>
    );
  }
}
