import React, { useState, useEffect } from "react";
// import axios from "axios";
import db from "../data/db.json";
import { Container, Row, Col } from "react-bootstrap";
import { Card, Dropdown, Menu } from "antd";
import { Link } from "react-router-dom";
import Img1 from "../assets/images/img2.png";
import Img2 from "../assets/images/user.png";
import Img3 from "../assets/images/img3.png";

//Components
// import PreLoading from "../containers/PreLoading";
let favorities = [];
export default function NewsDetails(props) {
  // const [loading, setLoader] = useState(false);
  const [newsDetails, setNewsDetails] = useState({});
  const [fav, setFav] = useState(false);

  const ChangeFav = f => {
    setFav(!fav);
  };

  useEffect(() => {
    setNewsDetails(
      db.articles.find(x => String(x.id) === String(props.match.params.id))
    );
  }, [props.match.params.id]);

  return (
    <div className="newsDetails pt-3">
      <Container className="homeNews ">
        {" "}
        <Row>
          <Col xs={8} className="prevLinks">
            <Link to="/" className="homeLink">
              Home
            </Link>{" "}
            <span className="px-3">></span>{" "}
            <Link to="/news" className="newsLink">
              News
            </Link>{" "}
            <span className="px-3">></span>{" "}
            <Link to="/news" className="detailsLink">
              News Details
            </Link>
            <h1> News Details</h1>
          </Col>
        </Row>
        {console.log(favorities)}
        <Card
          style={{ height: "100%" }}
          hoverable
          cover={<img alt="example" src={newsDetails.urlToImage} />}
        >
          <h3>
            {newsDetails.title}{" "}
            <span className="float-right">
              {fav ? (
                <i className="fas fa-heart" onClick={ChangeFav}></i>
              ) : (
                <i className="far fa-heart" onClick={ChangeFav}></i>
              )}
              <Dropdown
                overlay={
                  <Menu>
                    <Menu.Item>
                      <i className="fab fa-facebook-f"></i>
                      <i className="fab fa-twitter"></i>
                      <i className="fas fa-envelope"></i>
                    </Menu.Item>
                  </Menu>
                }
                trigger={["click"]}
              >
                <i className="fas fa-share-alt"></i>
              </Dropdown>
            </span>
          </h3>

          <p className="cardDesc">{newsDetails.description}</p>
        </Card>
        <h1 className="pt-4">Related Topics</h1>
        <Row>
          <Col sm={12} md={4} className="p-2">
            <img src={Img1} alt="related1" className="img-fluid" />{" "}
            <div className="imgLayer"></div>
            <div className="imgData">
              <p>Category</p>
              <h2>New Artificial Intelligence Apps</h2>
              <div className=" viewAllBtn">
                <Link to="/news">
                  View Details <i className="fas fa-long-arrow-alt-right"></i>
                </Link>
              </div>
            </div>
          </Col>
          <Col sm={12} md={4} className="p-2">
            <img src={Img2} alt="related1" className="img-fluid" />{" "}
            <div className="imgLayer"></div>
            <div className="imgData">
              <p>Category</p>
              <h2>New Artificial Intelligence Apps</h2>
              <div className=" viewAllBtn">
                <Link to="/news">
                  View Details <i className="fas fa-long-arrow-alt-right"></i>
                </Link>
              </div>
            </div>
          </Col>
          <Col sm={12} md={4} className="p-2">
            <img src={Img3} alt="related1" className="img-fluid" />
            <div className="imgLayer"></div>
            <div className="imgData">
              <p>Category</p>
              <h2>New Artificial Intelligence Apps</h2>
              <div className=" viewAllBtn">
                <Link to="/news">
                  View Details <i className="fas fa-long-arrow-alt-right"></i>
                </Link>
              </div>
            </div>
          </Col>{" "}
        </Row>{" "}
        <div className="text-right viewAllBtn py-4">
          <Link to="/news">
            view all <i className="fas fa-long-arrow-alt-right"></i>
          </Link>
        </div>
      </Container>
    </div>
  );
}
