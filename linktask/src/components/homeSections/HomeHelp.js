import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import location from "../../assets/images/locationIcon.png";
import time from "../../assets/images/timeIcon.png";
import zakat from "../../assets/images/zakatIcon.png";

export default function Homehelp() {
  return (
    <div className="homeHelp">
      {" "}
      <h1>How we have helped</h1>
      <h5>See how all foundation promoted change locally and to the world</h5>
      <Container className="mt-5">
        <Row>
          <Col lg={{ span: 9, offset: 2 }} md={{ span: 12, offset: 2 }}>
            <Row>
              <Col xl={3} xs={12} sm={6} md={6} className="py-5 px-5">
                <div className="square">
                  <div className="squareData pt-4 pl-4">
                    <img src={location} alt="icon" />
                    <p className="py-4">Find Place</p>
                    <p>+</p>
                  </div>
                </div>
              </Col>
              <Col xl={3} xs={12} sm={6} md={6} className="py-5 px-5">
                {" "}
                <div className="square">
                  <div className="squareData pt-4 pl-5">
                    <img src={location} alt="icon" />
                    <p className="py-4">A'awen</p>
                    <p>+</p>
                  </div>
                </div>
              </Col>
              <Col xl={3} xs={12} sm={6} md={6} className="py-5 px-5">
                {" "}
                <div className="square">
                  <div className="squareData pt-4 pl-5">
                    <img src={location} alt="icon" />
                    <p className="py-4">Omniyat</p>
                    <p>+</p>
                  </div>
                </div>
              </Col>
              <Col xl={3} xs={12} sm={6} md={6} className="py-5 px-5">
                {" "}
                <div className="square">
                  <div className="squareData pt-4 pl-4">
                    <img src={time} alt="icon" />
                    <p className="py-4">Give Time</p>
                    <p>+</p>
                  </div>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>{" "}
        <Row>
          <Col lg={{ span: 9, offset: 3 }} md={{ span: 12, offset: 3 }}>
            <Row>
              <Col xl={3} xs={12} sm={6} md={6} className="py-5 px-5">
                {" "}
                <div className="square">
                  <div className="squareData pt-4 pl-5">
                    <img src={location} alt="icon" />
                    <p className="py-4">Tofoula</p>
                    <p>+</p>
                  </div>
                </div>
              </Col>
              <Col xl={3} xs={12} sm={6} md={6} className="py-5 px-5">
                {" "}
                <div className="square">
                  <div className="squareData pt-4 pl-4">
                    <img src={location} alt="icon" />
                    <p className="py-4">Fundraising</p>
                    <p>+</p>
                  </div>
                </div>
              </Col>
              <Col xl={3} xs={12} sm={6} md={6} className="py-5 px-5">
                {" "}
                <div className="square">
                  <div className="squareData pt-4 pl-4">
                    <img src={zakat} alt="icon" />
                    <p className="py-4">Zakat</p>
                    <p>+</p>
                  </div>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
