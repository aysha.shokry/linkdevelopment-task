import React from "react";
import img1 from "../../assets/images/homeFooter.png";
import img2 from "../../assets/images/header2.png";
import img3 from "../../assets/images/header3.png";
import { Carousel, Button } from "react-bootstrap";
export default function HomeHeader() {
  return (
    <div className="homeHeader">
      <Carousel>
        <Carousel.Item>
          <img
            className="d-block w-100 img-fluid"
            src={img1}
            alt="First slide"
          />
          <Carousel.Caption>
            <h2>Al Foundation</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            <Button>Donate Now</Button>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100 img-fluid"
            src={img2}
            alt="Second slide"
          />

          <Carousel.Caption>
            <h2>Al Foundation</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            <Button>Donate Now</Button>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100 img-fluid"
            src={img3}
            alt="Third slide"
          />

          <Carousel.Caption>
            <h2>Al Foundation</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            <Button>Donate Now</Button>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
    </div>
  );
}
