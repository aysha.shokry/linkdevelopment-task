import React, { useState, useEffect } from "react";
// import axios from "axios";
import db from "../../data/db.json";
import { Container, Row, Col } from "react-bootstrap";
import { Button, Card, Dropdown, Menu } from "antd";
import { Link } from "react-router-dom";
//Components
// import PreLoading from "../containers/PreLoading";
export default function HomeNews() {
  // const [loading, setLoader] = useState(false);
  const [newsData, setNewsData] = useState([]);

  useEffect(() => {
    setNewsData(db.articles.filter(x => x.showOnHomepage));
  }, []);
  return (
    <Container className="homeNews pt-5">
      <Row>
        <Col xs={8}>
          <h1>Latest News</h1>
        </Col>
        <Col xs={4} className="text-right viewAllBtn">
          <Link to="/news">
            view all <i className="fas fa-long-arrow-alt-right"></i>
          </Link>
        </Col>
      </Row>
      <Row gutter={16} type="flex">
        {newsData && newsData.length !== 0
          ? newsData.map((n, index) => (
              <Col sm={12} md={6} lg={4} key={n.id} className="py-3">
                <Card
                  style={{ height: "100%" }}
                  hoverable
                  cover={
                    <img
                      alt="example"
                      src={n.urlToImage}
                      style={{ height: "160px" }}
                    />
                  }
                >
                  <p className="favIcons">
                    
                      <i
                        className="far fa-heart"
                      ></i>
                 
                    <Dropdown
                      overlay={
                        <Menu>
                          <Menu.Item>
                            <i className="fab fa-facebook-f"></i>
                            <i className="fab fa-twitter"></i>
                            <i className="fas fa-envelope"></i>
                          </Menu.Item>
                        </Menu>
                      }
                      trigger={["click"]}
                    >
                      <i className="fas fa-share-alt"></i>
                    </Dropdown>
                  </p>
                  <h4 className="cardTitle">{n.title}</h4>
                  <Button className="newsBtn">
                    <Link to={`/news/${n.id}`}>News</Link>
                  </Button>
                  <p className="cardDesc">{n.description.slice(0, 50)}</p>
                  <p className="cardDate">
                    <i className="far fa-calendar-alt pr-3"></i>
                    {new Date(n.publishedAt).toDateString()}
                  </p>
                </Card>
              </Col>
            ))
          : null}
      </Row>
    </Container>
  );
}
