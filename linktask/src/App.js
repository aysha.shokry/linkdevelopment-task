import React from "react";

//Style
import "./App.css";
import "./assets/styles/App.scss";

//Components
import HomePage from "./screens/HomePage";
import { BrowserRouter as Router, Route } from "react-router-dom";
import NavBar from "./containers/NavBar";
import NewsListing from "./screens/NewsListing";
import Footer from "./containers/Footer";
import NewsDetails from "./screens/NewsDetails";

function App() {
  return (
    <Router basename={process.env.PUBLIC_URL}>
      <div className="App">
        <NavBar />
        <Route exact path="/" component={HomePage} />{" "}
        <Route exact path="/news" component={NewsListing} />{" "}
        <Route exact path="/news/:id" component={NewsDetails} />
        <Footer />
      </div>
    </Router>
  );
}

export default App;
