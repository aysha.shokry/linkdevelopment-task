import React, { useState } from "react";
import { Navbar, Nav, Container, Row, Col } from "react-bootstrap";
import logo from "../assets/images/logo.png";
import user from "../assets/images/user.png";
import { Dropdown, Menu, Drawer } from "antd";
import { Link, NavLink } from "react-router-dom";
export default function NavBar() {
  const [visible, setsideMenuOpen] = useState(false);
  const [searchOpened, setSearchBar] = useState(false);
  const [placement] = useState("left");
  const openSearch = e => {
    setSearchBar(!searchOpened);
  };
  const showDrawer = () => {
    setsideMenuOpen(true);
  };

  const onClose = () => {
    setsideMenuOpen(false);
  };

  return (
    <div className="linkNav">
      <Navbar bg="light" fixed="top">
        <Container>
          <span>
            {visible ? (
              <i className="fas fa-times fa-2x mr-3" onClick={onClose}></i>
            ) : (
              <i className="fas fa-bars fa-2x mr-3" onClick={showDrawer}></i>
            )}
          </span>
          <Navbar.Brand>
            <Link to="/">
              <img alt="logo" src={logo} />
            </Link>
          </Navbar.Brand>
          {/* <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav"> */}
          <Nav className="ml-auto">
            <div className="Searchcontainer">
              <input
                placeholder="search..."
                className={searchOpened ? "searchOpened" : "searchClosed"}
                type="text"
              />
              <i className="fa fa-search" onClick={openSearch}></i>
            </div>
            <Dropdown
              className="notificationDrop"
              getPopupContainer={trigger => trigger.parentNode}
              trigger={["click"]}
              overlay={
                <Menu>
                  <p className="dropdownTitle">Recent Notifications</p>
                  <Menu.Item>
                    you have an upcoming appointment today!
                    <Link to="/Login" className="checkLink">
                      Check-in now
                    </Link>
                  </Menu.Item>
                  <hr />
                  <Menu.Item>Rate your last service appointment</Menu.Item>
                  <hr />
                  <Menu.Item>
                    Your appointment request has been approved
                  </Menu.Item>
                  <hr />
                  <Menu.Item className="allNotBtn">
                    <Link to="/">
                      All Notifications
                      <i className="fas fa-long-arrow-alt-right notificationArrow pl-2"></i>
                    </Link>
                  </Menu.Item>
                </Menu>
              }
              placement="bottomLeft"
              arrow
            >
              <i className="fas fa-bell navdropIcon navItem"></i>
            </Dropdown>
            <Nav.Link href="" className="userImg">
              <img
                style={{ width: "40px", borderRadius: "50%" }}
                className="img-fluid"
                src={user}
                alt="userPhoto"
              />
              <span> Name</span>
            </Nav.Link>
            <Dropdown
              className="settingsDrop"
              getPopupContainer={trigger => trigger.parentNode}
              trigger={["click"]}
              overlay={
                <Menu>
                  <p className="dropdownTitle">Language</p>
                  <Menu.Item>
                    <p>English</p>
                  </Menu.Item>
                  <Menu.Item>
                    <p>اللغة العربية</p>
                  </Menu.Item>
                  <hr />
                  <p className="dropdownTitle">Font Size</p>
                  <p>
                    <span style={{ fontSize: "30px", paddingRight: "3px" }}>
                      A
                    </span>
                    <span style={{ fontSize: "20px", paddingRight: "3px" }}>
                      A
                    </span>
                    <span style={{ fontSize: "15px", paddingRight: "3px" }}>
                      A
                    </span>
                  </p>
                  <hr />
                  <p className="dropdownTitle">Account</p>
                  <Menu.Item>Sign Out</Menu.Item>
                </Menu>
              }
              placement="bottomLeft"
              arrow
            >
              <i className="fas fa-cog navdropIcon navItem"></i>
            </Dropdown>
          </Nav>
        </Container>
        {/* </Navbar.Collapse> */}
      </Navbar>
      <Drawer
        className={visible ? "menuu" : ""}
        id="headerShown"
        placement={placement}
        closable={false}
        onClose={onClose}
        visible={visible}
        key={placement}
      >
        <div className="topSideMenu">
          <NavLink
            to="/"
            exact
            className="sideitem mr-lg-4 "
            activeClassName="navitem-active"
          >
            Home
          </NavLink>
          <p className=" mb-3"></p>
          <br />
          <NavLink
            to="/about"
            exact
            className="sideitem mr-lg-4"
            activeClassName="navitem-active"
          >
            About Us
          </NavLink>
          <p>Who are us</p>
          <p className=" mb-3">Why us?</p>
          <NavLink
            to="/news"
            exact
            className="sideitem mr-lg-4"
            activeClassName="navitem-active"
          >
            News
          </NavLink>
          <p>News</p>
          <p className=" mb-3">Events</p>
          <NavLink
            to="/careers"
            exact
            className="sideitem mr-lg-4"
            activeClassName="navitem-active"
          >
            Careers
          </NavLink>
          <p>Opportunities</p>{" "}
        </div>
        <div className="bottomSideMenu">
          <Row>
            <Col sm={3}>
              {" "}
              <ul>
                <li>
                  <i className="fab fa-twitter"></i>
                </li>
                <li>
                  <i className="fab fa-facebook-f"></i>
                </li>
                <li>
                  <i className="fab fa-linkedin"></i>
                </li>
                <li>
                  <i className="fab fa-youtube"></i>
                </li>
                <li>
                  <i className="fab fa-instagram"></i>
                </li>
              </ul>
            </Col>
            <Col sm={9}>
              {" "}
              <div
                className="ulDiv"
                style={{
                  paddingLeft: "15px",
                  bottom: "15px",
                  position: "absolute"
                }}
              >
                <NavLink
                  to="/careers"
                  exact
                  className="sideitem mr-lg-4"
                  activeClassName="navitem-active"
                >
                  Contact Us
                </NavLink>
                <br />
                <NavLink
                  to="/careers"
                  exact
                  className="sideitem mr-lg-4"
                  activeClassName="navitem-active"
                >
                  Site Map
                </NavLink>
              </div>
            </Col>
          </Row>
        </div>
      </Drawer>
    </div>
  );
}
