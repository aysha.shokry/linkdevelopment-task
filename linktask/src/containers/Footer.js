import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import { Input } from "antd";
export default function Footer() {
  return (
    <div className="footer">
      <Container className="footerLinks">
        <Row className="text-center">
          <Col xs={6} md={6} lg={3}>
            <ul>
              <li>
                <Link to="/news">News</Link>{" "}
              </li>
              <li>
                {" "}
                <Link to="/">Events</Link>{" "}
              </li>{" "}
              <li>
                <Link to="/">About</Link>{" "}
              </li>{" "}
              <li>
                <Link to="/">FAQ</Link>
              </li>
            </ul>
          </Col>{" "}
          <Col xs={6} md={6} lg={3}>
            <ul>
              <li>
                <Link to="/">Privacy Policy</Link>{" "}
              </li>{" "}
              <li>
                {" "}
                <Link to="/">Contact Us</Link>{" "}
              </li>{" "}
              <li>
                <Link to="/">Complains</Link>
              </li>
            </ul>
          </Col>
          <Col xs={12} md={6} lg={3}>
            <p className="text-left">Subscripe for newsletter</p>{" "}
            <Input.Search
              placeholder="Email Address"
              name="searchValue"
              enterButton="Subscribe"
            />
          </Col>
          <Col xs={12} md={6} lg={3}>
            <p className="text-left ml-md-5">Follow us on</p>
            <i className="fab fa-instagram"></i>
            <i className="fab fa-youtube"></i>
            <i className="fab fa-linkedin"></i>
            <i className="fab fa-facebook-f"></i>
            <i className="fab fa-twitter"></i>
          </Col>
        </Row>
      </Container>
      <div className="footerYear">
        All rights reserved - Link Development Company ©
        {new Date().getFullYear()}
      </div>
    </div>
  );
}
